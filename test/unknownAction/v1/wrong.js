'use strict';

function getHomeAddress() {
  let controller = () => {
    return true;
  };

  return {
    item: {
      controller
    },
    collection: {
      controller
    },
    thisIsWrong: {
      controller
    }
  };
}

module.exports = {
  getHomeAddress,
};
