'use strict';
const joi = require('joi');

function create() {
    let controller = (body) => {
        return {
          username: body.username,
          password: body.password,
          itWorks: true
        };
    };

    return {
        controller,
        middlewares: [
          () => Promise.resolve()
        ],
        validation: joi.object().keys({
          username: joi.string().min(3).required(),
          password: joi.string().min(8).required()
        })
    };
}

function list() {
    let controller = () => {
      return {
        results: [{
          id: 1,
          username: 'example'
        }]
      };
    };

    return {
        controller
    };
}

function get() {
    let controller = () => {
      return {
        id: 1,
        username: 'example'
      };
    };

    return {
        controller
    };
}

function update() {
    let controller = () => {
        return true;
    };

    return {
        controller,
        middlewares: [
          function(req){
            // change a value for the tests
            req.nbMiddleware = 1;
          },
          function(req){
            // change a value for the tests
            req.nbMiddleware++;
          },
          function(req){
            // change a value for the tests
            req.nbMiddleware++;
          }
        ]
    };
}

function patch() {
    let controller = () => {
        return true;
    };

    return {
        controller,
        middlewares: [
          function(req){
            // change a value for the tests
            req.wentHere = true;
          }
        ]
    };
}

function del() {
    let controller = () => {
        return;
    };

    return {
        controller,
        middlewares: [
          function(){
            throw new Error('this is for the tests');
          }
        ]
    };
}

function hire() {
    let controller = () => {
        return true;
    };

    return {
        item: {
            controller
        },
        collection: {
            controller
        }
    };
}

function getHomeAddress() {
    let controller = () => {
        return true;
    };

    return {
        item: {
            controller
        },
        collection: {
            controller
        }
    };
}

function sendEmail() {
  let controller = () => {
      return true;
  };

  return {
      item: {
          controller
      }
  };
}

function lookUp() {
  let controller = () => {
      return true;
  };

  return {
      collection: {
          controller
      }
  };
}


module.exports = {
    create: create,
    list: list,
    get: get,
    update: update,
    patch: patch,
    del: del,
    hire: hire,
    getHomeAddress: getHomeAddress,
    sendEmail: sendEmail,
    lookUp: lookUp
};
